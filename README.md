<H1>Proof of Concept - Mail Parser</H1>

<H3>Objectives:</H3>

<br>
-- EMAIL LISTING --
<br><br>

1. Access inbox given an <b>email address</b> (Gmail only) and <b>app-password</b>

2. List emails given specific <b>date range</b>

3. Group emails by specific <b>sender, subject, tags/labels</b>

<br>
-- ATTACHMENT PROCESSING --
<br><br>

4. Decoding zip files

5. Unzipping (with/without password-protected) files

6. Open (with/without password-protected) files

<br>
-- FILE FORWARDING --
<br><br>

7. Save file contents to another file and remove its password protection

8. Forward file using external API
